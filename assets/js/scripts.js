/**
 * Created by cram on 19.07.2016.
 */
(function () {
    refreshInterface();
    $('#login').on('submit', function (e) {
        e.preventDefault();
        var data=$(this).serialize();
        $.ajax('/users/login',
            {
                type: 'POST',
                data: data,
                success: function (result) {
                    console.log(result);
                    refreshInterface();
                },
                error: function (result) {
                    $('.error','#login').show();
                },
                xhrFields: {
                    withCredentials: true
                }
            }
        )
    });

    $('#logout').on('click', function (e) {
        e.preventDefault();
        $.ajax('/users/logout', {
            success: function (result) {
                $.removeCookie('token');
                refreshInterface();
            }
        })
    });

    $('#register').on('submit', function (e) {
        e.preventDefault();
        var data=$(this).serialize();
        $.ajax('/users/register',
            {
                type: 'POST',
                data: data,
                success: function (result) {
                    console.log(result);
                    refreshInterface();
                },
                error: function (result) {
                    $('#register .has-error').removeClass('has-error');
                    $('.error', '#register').hide();
                    $.each(JSON.parse(result.responseText), function (k, v) {
                        if(typeof (v)=='object')
                            v=v.join('. ');
                        var $node=$('#error'+k);
                        $node.parent().addClass('has-error');
                        $node.html(v);
                        $node.show();
                    })
                },
                xhrFields: {
                    withCredentials: true
                }
            }
        )
    });

    function refreshInterface() {
        if(typeof($.cookie('token'))!=='undefined') {
            $('#loginForm').hide();
            $('#profile').show();

            $.ajax('/users/profile', {
                dataType: 'json',
                success: function (result) {
                    console.log(result);
                    $('dd#profilename').html(result.name);
                    $('dd#profileusername').html(result.username);
                }
            });
        } else {
            $('#loginForm').show();
            $('#profile').hide();
        }
        $('.error').hide();
        $('#login')[0].reset();
        $('#register')[0].reset();
    }
})(jQuery);