<?php
/**
 * Created by PhpStorm.
 * User: cram
 * Date: 19.07.2016
 * Time: 12:28
 */
class SiteController extends Controller {

    public function __construct() {
        array_push($this->models,'Users');
        parent::__construct();
    }

    public function actionIndex() {
        $users=Users::first();

        $this->view->render('main');
    }
}