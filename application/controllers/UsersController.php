<?php
/**
 * Created by PhpStorm.
 * User: cram
 * Date: 19.07.2016
 * Time: 14:01
 */
class UsersController extends Controller {
    public function actionLogin() {
        if($this->isAjaxRequest()) {
            if(Users::login($_POST))
                return true;
        }
        http_response_code(404);
        return false;
    }

    public function actionLogout() {
        if($this->isAjaxRequest()) {
            if (isset($_COOKIE['token'])) {
                $session = Sessions::first(array('token' => $_COOKIE['token']));
                if ($session)
                    $session->delete();
                unset($_COOKIE['token']);
                return true;
            }
        }
        return false;
    }

    public function actionRegister () {
        if($this->isAjaxRequest()) {
            $user=Users::register($_POST);
            if($user->is_valid()) {
                return true;
            } else {
                $errors=[];
                if($user->errors->on('name')) $errors['name']=$user->errors->on('name');
                if($user->errors->on('username')) $errors['username']=$user->errors->on('username');
                if($user->errors->on('password')) $errors['password']=$user->errors->on('password');
                http_response_code(404);
                echo json_encode($errors);
                return false;
            }
        }
        http_response_code(403);
        return false;
    }

    public function actionProfile () {
        if($this->isAjaxRequest() && isset($_COOKIE['token'])) {
            $session=Sessions::first(array('token'=>$_COOKIE['token']));
            if($session) {
                $attr=$session->attributes();
                $user=Users::first($attr['user_id']);
                if($user) {
                    $output=$user->attributes();
                    unset($output['password']);
                    echo json_encode($output);
                    return true;
                }
            }
        }
        http_response_code(403);
        return false;
    }
}