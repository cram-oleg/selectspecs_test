<?php
/**
 * Created by PhpStorm.
 * User: cram
 * Date: 19.07.2016
 * Time: 12:20
 */
class View
{

    public function render($content_view, $layout='layout', $data = null) {
        include 'application/views/'.$layout.'.php';
    }
}