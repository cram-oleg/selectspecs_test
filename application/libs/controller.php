<?php
/**
 * Created by PhpStorm.
 * User: cram
 * Date: 19.07.2016
 * Time: 12:20
 */
class Controller {

    public $model;
    public $view;
    public $models=['Sessions'];

    public function __construct() {
        $this->view = new View();
    }

    public function actionIndex() {
        //Some default staff
    }

    public function actionNotFound() {
        $this->view->render('notFound');
    }

    public function isAjaxRequest () {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            return true;
        }
        return false;
    }
}