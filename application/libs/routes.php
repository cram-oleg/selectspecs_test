<?php
/**
 * Created by PhpStorm.
 * User: cram
 * Date: 19.07.2016
 * Time: 12:20
 */
class Routes
{
    public static function start()
    {
        // Default controller and action
        $controller_name = 'Site';
        $action_name = 'index';

        $routes = explode('/', $_SERVER['REQUEST_URI']);

        if ( !empty($routes[1]) ) {
            $controller_name = $routes[1];
        }
        if ( !empty($routes[2]) ) {
            $action_name = $routes[2];
        }

        if($controller_name=='404') {
            $controller_name='Site';
            $action_name='notFound';
        }

        $model_name = ucfirst($controller_name);
        $controller_name = ucfirst($controller_name).'Controller';
        $action_name = 'action'.ucfirst($action_name);

        $model_file = ucfirst(strtolower($model_name)).'.php';
        $model_path = "application/models/".$model_file;

        if(file_exists($model_path))
        {
            include "application/models/".$model_file;
        }

        $controller_file = $controller_name.'.php';
        $controller_path = "application/controllers/".$controller_file;
        if(file_exists($controller_path)) {
            include "application/controllers/".$controller_file;
        } else {
            static::ErrorPage404();
        }

        $controller = new $controller_name;

        if(count($controller->models)) {
            foreach($controller->models as $model) {
                include "application/models/" . $model . '.php';
            }
        }

        if(method_exists($controller, $action_name)) {
            $controller->$action_name();
        } else {
            static::ErrorPage404();
        }
    }

    public static function ErrorPage404()
    {
        $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        header('Location:'.$host.'404');
        exit();
    }
}