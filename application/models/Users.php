<?php
/**
 * Created by PhpStorm.
 * User: cram
 * Date: 19.07.2016
 * Time: 13:03
 */
class Users extends ActiveRecord\Model {

    private static $username, $password, $id, $access_token;

    static $validates_presence_of = array(
        array('username'),
        array('password'),
        array('name')
    );

    public static $validates_format_of = array(
        array('password',
            'with' => '/(?=[-_a-zA-Z0-9]*?[A-Z])(?=[-_a-zA-Z0-9]*?[a-z])(?=[-_a-zA-Z0-9]*?[0-9])[-_a-zA-Z0-9]{6,30}/',
            'message' => 'Must contain at least one Upper, one Lower and one number'
        )
    );
    public static $validates_uniqueness_of = array(
        array('username')
    );

    public static function register($data) {
        $user=new Users();
        $user->name=$data['name'];
        $user->username=$data['username'];
        $user->password=$data['password'];

        if($user->is_valid()) {
            $user->password=static::hashPassword($user->password);
            $user->save(false);  //Now skip validation rules
            Users::login($data);
            return true;
        }
        return $user;
    }

    public static function login($data) {
        self::$username=isset($data['username']) ? $data['username'] : '';
        self::$password=isset($data['password']) ? self::hashPassword($data['password']) : '';
        $user=Users::first(array('username' => self::$username, 'password'=>self::$password));
        if($user) {
            $attr=$user->attributes();
            $session=new Sessions();
            $access_token=md5(uniqid());
            $session->token=$access_token;
            $session->user_id=$attr['id'];
            $session->save();
            setcookie('token', $access_token, time()+60*60, '/');
            return true;
        }
        return false;
    }


    private static function hashPassword($pass) {
        return md5($pass);
    }
}