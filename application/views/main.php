<div class="row" id="loginForm">
    <div class="col-sm-6">
        <h2>Login:</h2>
        <form id="login">
            <div class="alert alert-danger error" role="alert" style="display:none">Incorrect username or password</div>
            <div class="form-group">
                <label for="loginUserName">Username</label>
                <input type="text" class="form-control" name="username" id="loginUserName" placeholder="Enter Username">
            </div>
            <div class="form-group">
                <label for="loginPassword">Password</label>
                <input type="password" class="form-control" name="password" id="loginPassword" placeholder="Enter Password">
            </div>

            <button type="submit" class="btn btn-primary">Login</button>
        </form>
    </div>
    <div class="col-sm-6">
        <h2>Register:</h2>
        <form id="register">
            <div class="form-group">
                <label for="registerUserName">Username</label>
                <input type="text" class="form-control" name="username" id="registerUserName" placeholder="Enter Username">
                <span id="errorusername" class="help-block error"></span>
            </div>
            <div class="form-group">
                <label for="registerName">Name</label>
                <input type="text" class="form-control" name="name" id="registerName" placeholder="Enter Name and Sirname">
                <span id="errorname" class="help-block error"></span>
            </div>
            <div class="form-group">
                <label for="loginPassword">Password</label>
                <input type="password" class="form-control" name="password" id="registerPassword" placeholder="Enter Password">
                <span id="errorpassword" class="help-block error"></span>
            </div>

            <button type="submit" class="btn btn-success">Register</button>
        </form>

    </div>
</div>
<div class="col-sm-6" id="profile">
    <div class="panel panel-primary">
        <div class="panel-heading">Your profile:</div>
        <div class="panel-body">
            <dl class="dl-horizontal">
                <dt>Username:</dt>
                <dd id="profileusername"></dd>
                <dt>Name:</dt>
                <dd id="profilename"></dd>
            </dl>
        </div>
    </div>
    <a href="#" id="logout" class="btn btn-success">Logout</a>
</div>