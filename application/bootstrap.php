<?php
/**
 * Created by PhpStorm.
 * User: cram
 * Date: 19.07.2016
 * Time: 12:19
 */
require_once 'libs/model.php';
require_once 'libs/view.php';
require_once 'libs/controller.php';
require_once 'libs/routes.php';

require_once 'config/db.php';

require_once 'libs/vendor/php-activerecord/ActiveRecord.php';

ActiveRecord\Config::initialize(function($cfg) use ($connections) {
    $cfg->set_model_directory('application/models');
    $cfg->set_connections($connections);
});

Routes::start();